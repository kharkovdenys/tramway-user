import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Travelcard } from './Travelcard.entity';

@Entity({ name: 'Type_card' })
export class Type_card {
    @PrimaryGeneratedColumn()
    Type_card_id: number;

    @Column()
    Type_name_ua: string;

    @Column()
    Type_name_en: string;

    @OneToMany(() => Travelcard, travelcard => travelcard.type_card)
    travelcards: Travelcard[];
}
