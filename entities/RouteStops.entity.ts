import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Stop } from './Stop.entity';
import { TravelHistory } from './TravelHistory.entity';

@Entity({ name: 'Route_stops' })
export class RouteStops {
    @PrimaryGeneratedColumn()
    Route_stops_id: number;

    @Column()
    Route_id: number;

    @Column()
    Stop_id: number;

    @Column()
    Stop_number: number;

    @Column()
    Duration: number;

    @Column()
    Unnecessary: boolean;

    @ManyToOne(() => Stop, stop => stop.route_stops)
    @JoinColumn({ name: 'Stop_id' })
    stop: Stop;

    @OneToMany(() => RouteStops, routeStops => routeStops.stop)
    travelHistoryStart: TravelHistory[];

    @OneToMany(() => RouteStops, routeStops => routeStops.stop)
    travelHistoryEnd: TravelHistory[];
}
