import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { RouteStops } from './RouteStops.entity';

@Entity({ name: 'Stop' })
export class Stop {
    @PrimaryGeneratedColumn()
    Stop_id: number;

    @Column()
    Stop_name_ua: string;

    @Column()
    Stop_name_en: string;

    @OneToMany(() => RouteStops, routeStops => routeStops.stop)
    route_stops: RouteStops[];
}
