import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
} from 'typeorm';
import { RouteStops } from './RouteStops.entity';

@Entity({ name: 'Travel_history' })
export class TravelHistory {
    @PrimaryGeneratedColumn()
    Travel_id: number;

    @Column()
    Travelcard_id: number;

    @Column()
    Route_start_id: number;

    @Column({ nullable: true })
    Route_end_id: number | null;

    @Column()
    Reverse: boolean;

    @Column({ type: 'datetime' })
    Date_of_start: Date;

    @ManyToOne(() => RouteStops, routeStops => routeStops.travelHistoryStart)
    @JoinColumn({ name: 'Route_start_id' })
    routeStart: RouteStops;

    @ManyToOne(() => RouteStops, routeStops => routeStops.travelHistoryEnd)
    @JoinColumn({ name: 'Route_end_id' })
    routeEnd: RouteStops;
}
