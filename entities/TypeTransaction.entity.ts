import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('Type_transaction')
export class TypeTransaction {
    @PrimaryColumn()
    Type_id: number;

    @Column('varchar', { length: 50 })
    Type_name_ua: string;

    @Column('varchar', { length: 50 })
    Type_name_en: string;
}
