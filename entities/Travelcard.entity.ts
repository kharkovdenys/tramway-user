import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, ManyToOne, JoinColumn } from 'typeorm';
import { Users } from './Users.entity';
import { Type_card } from './Type_card.entity';

class ColumnNumericTransformer {
    to(data: number): number {
        return data;
    }
    from(data: string): number {
        return parseFloat(data);
    }
}

@Entity({ name: 'Travelcard' })
export class Travelcard {
    @PrimaryGeneratedColumn()
    Travelcard_id: number;

    @Column()
    Type_card_id: number;

    @Column()
    First_name: string;

    @Column()
    Last_name: string;

    @Column()
    IPN: string;

    @Column('decimal', { precision: 8, scale: 2, transformer: new ColumnNumericTransformer() })
    Balance: number;

    @ManyToOne(() => Type_card, type_card => type_card.travelcards)
    @JoinColumn({ name: 'Type_card_id' })
    type_card: Type_card;

    @OneToOne(() => Users, user => user.travelcard)
    users: Users[];
}
