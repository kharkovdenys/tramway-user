import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { TypeTransaction } from './TypeTransaction.entity';



@Entity('Transaction')
export class Transaction {
    @PrimaryGeneratedColumn()
    Transaction_id: number;

    @Column('int')
    Travelcard_id: number;

    @Column('int')
    Type_id: number;

    @Column('datetime')
    Date: Date;

    @Column('decimal', { precision: 8, scale: 2 })
    Change: number;

    @ManyToOne(() => TypeTransaction, { eager: true })
    @JoinColumn({ name: 'Type_id' })
    Type: TypeTransaction;
}
