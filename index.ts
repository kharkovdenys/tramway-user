import cors from 'cors';
import express from 'express';
import moment from 'moment';

import { AppDataSource } from './data-source';
import { Route } from './entities/Route.entity';
import { RouteStops } from './entities/RouteStops.entity';
import { Schedule } from './entities/Schedule.entity';
import { Transaction } from './entities/Transaction.entity';
import { Travelcard } from './entities/Travelcard.entity';
import { TravelHistory } from './entities/TravelHistory.entity';
import { Users } from './entities/Users.entity';
import { authenticateToken, generateToken, hashPassword } from './services/auth';

const app = express();
const port = 3001;

app.use(express.json());
app.use(cors());

app.listen(port, async () => {
    await AppDataSource.initialize();
    console.log(`Server is running on port ${port}`);
});

app.post('/login', async (req, res) => {
    const { username, password } = req.body;

    try {
        const user = await AppDataSource.getRepository(Users).findOne({ where: { Username: username } });

        if (!user || hashPassword(password) !== user.Password) {
            return res.status(401).json({ message: 'Invalid username or password' });
        }

        const token = generateToken(user.User_id, user.Username, user.Travelcard_id);
        res.json({ token });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/travel_history', authenticateToken, async (req: any, res: any) => {
    res.json(await AppDataSource.getRepository(TravelHistory).find({ where: { Travelcard_id: req.user.travelcardID }, relations: { routeEnd: { stop: true }, routeStart: { stop: true } }, select: { Date_of_start: true, Reverse: true, Travel_id: true } }));
});

app.get('/transaction_history', authenticateToken, async (req: any, res: any) => {
    res.json(await AppDataSource.getRepository(Transaction).find({ where: { Travelcard_id: req.user.travelcardID }, relations: { Type: true } }));
});

app.get('/info', authenticateToken, async (req: any, res: any) => {
    res.json(await AppDataSource.getRepository(Travelcard).find({ where: { Travelcard_id: req.user.travelcardID }, relations: { type_card: true } }));
});

app.post('/top_up', authenticateToken, async (req: any, res) => {
    const { add, type_id } = req.body;

    try {
        if (add <= 0) {
            throw Error('Мінувосе значення');
        }
        await AppDataSource.transaction(async (transactionalEntityManager) => {
            await transactionalEntityManager.query(
                'UPDATE Travelcard SET Balance = Balance + @0 WHERE Travelcard_id = @1',
                [add, req.user.travelcardID]
            );
            await transactionalEntityManager.query('INSERT INTO [Transaction] VALUES(@0, @1, @2, @3)', [req.user.travelcardID, type_id, new Date(Date.now()), add]);
        });
        res.send();
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/qr', authenticateToken, async (req: any, res) => {
    try {
        const travelcard = await AppDataSource.getRepository(Travelcard).findOne({ where: { Travelcard_id: req.user.travelcardID } });
        if (!travelcard || travelcard.Balance < 16) {
            throw Error('Недостатньо балансу');
        }
        await AppDataSource.transaction(async (transactionalEntityManager) => {
            const currentDate = new Date();
            const newDate = new Date(currentDate.setDate(currentDate.getDate() + 5));
            await transactionalEntityManager.query('INSERT INTO QR_ticket VALUES(NEWID(), @0, @1, @2, @3, @4, @5)', [newDate, 0, null, null, null, req.user.travelcardID]);
            await transactionalEntityManager.query(
                'UPDATE Travelcard SET Balance = Balance - @0 WHERE Travelcard_id = @1',
                [16, req.user.travelcardID]
            );
            await transactionalEntityManager.query('INSERT INTO [Transaction] VALUES(@0, @1, @2, @3)', [req.user.travelcardID, 8, new Date(Date.now()), -16]);
        });
        res.send();
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/my_qr', authenticateToken, async (req: any, res: any) => {
    res.json(await AppDataSource.manager.query('SELECT * FROM QR_ticket WHERE Travelcard_id = @0', [req.user.travelcardID]));
});

app.get('/routes', authenticateToken, async (req: any, res: any) => {
    res.json(await AppDataSource.getRepository(Route).find());
});

app.get('/schedule/:id', authenticateToken, async (req, res) => {
    try {
        const routeId = parseInt(req.params.id);

        const schedule = await AppDataSource.getRepository(Schedule).find({ where: { Route_id: routeId } });

        const routeStops = await AppDataSource.getRepository(RouteStops).find({
            where: { Route_id: routeId },
            relations: { stop: true },
        });

        const currentTime = moment();
        currentTime.set('year', 1970);
        currentTime.set('month', 0);
        currentTime.set('date', 1);

        const arrivalTimes = routeStops.map((stop) => {
            const totalDuration = calculateTotalDuration(routeStops, stop);
            const matchingSchedule = schedule.filter((s) => s.Reverse === false);
            var arrivalTimes: string[] = [];
            for (var i = 0; i < matchingSchedule.length; i++) {
                const startTime = moment(matchingSchedule[i].Time_start, 'HH:mm:ss');
                const arrivalTime = startTime.add(totalDuration, 'minutes');
                if (arrivalTime.diff(currentTime, 'seconds') > 0) {
                    arrivalTimes.push(arrivalTime.format('HH:mm:ss'));
                    if (arrivalTimes.length >= 2) break;
                }
            }
            const totalDurationReverse = calculateTotalDurationReverse(routeStops, stop);
            const matchingScheduleReverse = schedule.filter((s) => s.Reverse === true);
            var arrivalTimesReverse: string[] = [];
            for (var i = 0; i < matchingScheduleReverse.length; i++) {
                const startTime = moment(matchingScheduleReverse[i].Time_start, 'HH:mm:ss');
                const arrivalTime = startTime.add(totalDurationReverse, 'minutes');
                if (arrivalTime.diff(currentTime, 'seconds') > 0) {
                    arrivalTimesReverse.push(arrivalTime.format('HH:mm:ss'));
                    if (arrivalTimesReverse.length >= 2) break;
                }
            }

            return {
                number: stop.Stop_number,
                stop: stop.stop,
                arrivalTime: arrivalTimes,
                arrivalTimeReverse: arrivalTimesReverse
            };
        });

        res.json(arrivalTimes);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

function calculateTotalDuration(stops: RouteStops[], currentStop: RouteStops) {
    let totalDuration = 0;
    for (const stop of stops) {
        if (stop.Stop_number < currentStop.Stop_number + 1) {
            totalDuration += stop.Duration;
        }
    }
    return totalDuration;
}
function calculateTotalDurationReverse(stops: RouteStops[], currentStop: RouteStops) {
    let totalDuration = 0;
    for (const stop of stops) {
        if (stop.Stop_number > currentStop.Stop_number) {
            totalDuration += stop.Duration;
        }
    }
    return totalDuration;
}
