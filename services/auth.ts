import * as crypto from 'crypto';
import { Response } from 'express';
import * as jwt from 'jsonwebtoken';


export function hashPassword(password: string): string {
    const sha256 = crypto.createHash('sha256');
    sha256.update(password);
    return sha256.digest('hex');
}

export function generateToken(userId: number, username: string, travelcardID: number): string {
    const secretKey = '_13_chah_lich_13_';
    const expiresIn = '1d';

    return jwt.sign({ userId, username, travelcardID }, secretKey, { expiresIn });
}

export function authenticateToken(req: any, res: Response, next: () => void) {
    const token = req.header('Authorization');
    if (!token) return res.status(401).json({ message: 'Unauthorized' });

    jwt.verify(token, '_13_chah_lich_13_', (err: any, user: any) => {
        if (err) return res.status(403).json({ message: 'Forbidden' });
        req.user = user;
        next();
    });
}
